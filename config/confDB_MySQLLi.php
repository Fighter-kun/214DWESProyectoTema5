<?php
/**
 * @author Carlos García Cachón
 * @version 1.0 
 * @since 20/11/2023
 * Archivo de configuración de la BD del Instituto para MySQLLi
 */
define('DSN', '192.168.20.19'); // Dirección del servidor
define('DBNAME', 'DB214DWESProyectoTema4'); // Nombre de la base de datos
define('USERNAME', 'user214DWESProyectoTema4'); // Nombre de usuario de la base de datos
define('PASSWORD', 'paso'); // Contraseña de la base de datos

